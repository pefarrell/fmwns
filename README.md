A solver for the stationary incompressible Navier--Stokes
equations with Reynolds-robust preconditioner.

Described in Farrell, Mitchell and Wechsung (2018).

This code has been superseded by alfi: https://github.com/florianwechsung/alfi .
