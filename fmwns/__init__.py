from fmwns.problemclass import NavierStokesProblem
from fmwns.solver import NavierStokesSolver
from fmwns.prolongation import CoarseCellPatches
