# import matplotlib
# matplotlib.use('PDF')
import matplotlib.pyplot as plt
import numpy as np

plt.figure(figsize=(8/2.54, 8/2.54))
ax = plt.gca()
# plt.rcParams['text.latex.preamble'] = [r"\usepackage{lmodern}"]
plt.rcParams["font.family"] = ["Latin Modern Roman"]
params = {'text.usetex': True,
          'font.size': 7,
          'font.family': 'Latin Modern Roman',
          'text.latex.unicode': True,
          'mathtext.fontset': 'cm'
          }
plt.rcParams.update(params)

Re = 1
errors_velocity = [[0.04286857784298428, 0.011735658163399259, 0.003000858381542257, 0.0007471976130793737],
                   [0.04286857402204187, 0.01173565558156694, 0.003000863220298873, 0.0007471996648616465]]
errors_pressure = [[4.270408558307532, 1.45129740260904, 0.5602173788374119, 0.24719573090738284],
                   [4.270411304983216, 1.451295801713875, 0.5602187361087847, 0.24720166676410435]]
h = [0.125, 0.0625, 0.03125, 0.015625]
gammas = [r"10^4", r"1"]


Re = 200
errors_velocity = [[0.13215858082800036, 0.050334347128739035, 0.014943672269993819, 0.003907725899355334],
                   [0.13215856601473555, 0.05033432316160743, 0.014943633361034665, 0.003908868838482483]]
errors_pressure = [[0.06845836913009083, 0.028509822141724518, 0.012237923582738626, 0.005695146735485956],
                   [0.06845869944518267, 0.028509864110109282, 0.012238694907580143, 0.005695999382163473]]
h = [0.125, 0.0625, 0.03125, 0.015625]
gammas = [r"10^4", r"1"]

Re = 500
errors_velocity = [[0.2995336464210087, 0.18202494743173328, 0.07424232186164026, 0.021846173493383855],
                   [0.2995336668276712, 0.18202498950588136, 0.07424288469796395, 0.02184367467705331]]
errors_pressure = [[0.09052980098930472, 0.04879634751513032, 0.020192499273121542, 0.007241290099354369],
                   [0.09052973437726483, 0.04879621859887206, 0.02019255429601628, 0.007242366703815572]]
h = [0.125, 0.0625, 0.03125, 0.015625]
gammas = [r"10^4", r"1"]


for i in range(len(errors_velocity)):
    marker = "x" if i == 0 else "o"
    lt = "-" if i == 0 else "--"
    ms = 4 if i == 0 else 5
    plt.loglog(h, errors_pressure[i], lt, marker=marker, markersize=ms,
               markeredgewidth=0.5, markerfacecolor='None', linewidth=0.5,
               label=r"$\|p-p_h\|_{L^2}$, $\gamma=" + gammas[i] + r"$")
    plt.loglog(h, errors_velocity[i], lt, marker=marker, markersize=ms,
               markeredgewidth=0.5, markerfacecolor='None', linewidth=0.5,
               label=r"$\|u-u_h\|_{L^2}$, $\gamma=" + gammas[i] + r"$")

fak = 2. if errors_pressure[0][0] > errors_velocity[0][0] else 0.5
x = np.asarray(h)
y = x**(1)
y = y*errors_pressure[0][0] * fak/y[0]
plt.plot(x, y, "--", label=r"$h$", linewidth=0.5)
y = x**(2)
fak = 0.2 if Re == 500 else fak
y = y*errors_velocity[0][0]/(fak*y[0])
plt.plot(x, y, ":", label=r"$h^2$", linewidth=0.5)
plt.xlabel(r"$h$")
plt.ylabel(r"$L^2\mathrm{-\,error}$")
plt.legend(loc="lower right")
plt.title(r"$\mathrm{Convergence\ for\ Re=%i}$" % Re)
plt.tight_layout(pad=0.)

plt.xscale('log', basex=2)
plt.savefig("mms-re-%i_p1fb.pdf" % Re, dpi=300)
