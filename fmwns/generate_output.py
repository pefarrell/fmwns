import os

def generate_output(info_dict, runlabel):
    os.makedirs("output", exist_ok=True)
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    plt.figure()
    f, (ax1, ax2, ax3) = plt.subplots(3)
    res = [d["Re"] for d in info_dict]
    linear_iter = [d["linear_iter"] for d in info_dict]
    non_linear_iter = [d["nonlinear_iter"] for d in info_dict]
    times = [d["time"] for d in info_dict]

    ax1.semilogx(res, linear_iter, label="Linear iterations")
    ax1.legend()
    ax2.semilogx(res, [a/b for a, b in zip(linear_iter, non_linear_iter)],
                 label="Linear iterations per nonlinear iteration")
    ax2.legend()
    ax3.semilogx(res, times, label="Time")
    ax3.legend()
    iter_summary = ["%i: %i" % (i, linear_iter[res.index(i)])
                    for i in sorted(set([100,
                                         1000, 10000]).intersection(set(res)))]

    plt.suptitle("Results for run " + runlabel + "\nIts: "
                 + ", ".join(iter_summary) + " Time: " + ("%.2f" % sum(times)))
    plt.savefig("output/results_" + runlabel + ".pdf")
