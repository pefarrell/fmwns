from firedrake import *
from fmwns import *
import numpy
import os


class ThreeDimBackwardsFacingStepProblem(NavierStokesProblem):

    def mesh(self, distribution_parameters):
        return Mesh("unstructured-backwards-facing-step-3d.msh", distribution_parameters=distribution_parameters)

    @staticmethod
    def poiseuille_flow(domain):
        (x, y, z) = SpatialCoordinate(domain)
        return as_vector([16 * (2-y)*(y-1)*z*(1-z)*(y>1), 0, 0])

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), self.poiseuille_flow(Z.ufl_domain()), 1),
               DirichletBC(Z.sub(0), Constant((0., 0., 0.)), 3)]
        return bcs

    def has_nullspace(self): return False


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("--nref", type=int, default=1)
    parser.add_argument("--vel_degree_wo_bubble", type=int, default=1)
    parser.add_argument("--supg_weight", type=float, default=0.05)
    parser.add_argument("--paraview", type=int, default=1)
    parser.add_argument("--full_checkpoints", type=int, default=1)
    args, _ = parser.parse_known_args()

    nref_viz = 1 if args.paraview == 1 else 0
    problem = ThreeDimBackwardsFacingStepProblem()
    solver = NavierStokesSolver(problem, multigrid=True, nref=args.nref, gamma=10000, nref_viz=nref_viz,
                                vel_degree_wo_bubble=args.vel_degree_wo_bubble, supg_weight=Constant(args.supg_weight))
    comm = solver.mesh.mpi_comm()

    if args.paraview == 1:
        upvdf = File("output/velocity-%i.pvd" % solver.Z.dim())
        ppvdf = File("output/pressure-%i.pvd" % solver.Z.dim())
    os.makedirs("checkpoint", exist_ok=True)
    start = 200
    end = 5000
    step = 100
    res = list(range(0, 101, 10)) + list(range(start, end+step, step))
    if args.full_checkpoints == 1:
        checkpoints = res
    else:
        checkpoints = [0, 10, 100, 500] + list(range(1000, end+1000, 1000))
    results = []
    for re in res:
        try:
            with DumbCheckpoint("checkpoint/nssolution-dofs-%s-Re-%s" % (solver.Z.dim(), re), mode=FILE_READ) as checkpoint:
                checkpoint.load(solver.z, name="up_%i" % re)
            if args.paraview == 1:
                upvdf.write(solver.vis_velocity(solver.z.split()[0]), time=re)
                ppvdf.write(solver.z.split()[1], time=re)
            continue
        except:
            pass
        (z, info_dict) = solver.solve(re)
        results.append(info_dict)
        if re in checkpoints:
            with DumbCheckpoint("checkpoint/nssolution-dofs-%s-Re-%s" % (solver.Z.dim(), re), mode=FILE_UPDATE) as checkpoint:
                checkpoint.store(solver.z, name="up_%i" % re)
        if args.paraview == 1:
            upvdf.write(solver.vis_velocity(solver.z.split()[0]), time=re)
            ppvdf.write(solver.z.split()[1], time=re)

    # Otherwise, some things are not collected (This lives inside a
    # DM, but holds a reference to the DM itself)
    solver.prolongation.break_ref_cycles()
    if comm.rank == 0:
        for info_dict in results:
            print(info_dict)
