from firedrake import *
from fmwns import *
import numpy as np
import os

convergence_orders = lambda x: np.log2(np.array(x)[:-1] / np.array(x)[1:])


class ThreeDimLidDrivenCavityMMSProblem(NavierStokesProblem):
    def __init__(self, baseN, Re=100):
        super().__init__()
        self.baseN = baseN
        self.Re = Re

    def mesh(self, distribution_parameters):
        mesh = BoxMesh(self.baseN, self.baseN, self.baseN, 2, 2, 2,
                       distribution_parameters=distribution_parameters)
        return mesh

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), self.actual_solution(Z)[0], [4, 5, 6]),
               DirichletBC(Z.sub(0), Constant((0., 0., 0.)), [1, 2, 3])]
        return bcs

    def has_nullspace(self): return True

    def interpolate_initial_guess(self, z):
        w_expr = self.actual_solution(z)[0]
        z.sub(0).interpolate(w_expr)

    def char_length(self): return 2.0

    def actual_solution(self, Z):
        # Taken from 'EFFECTS OF GRID STAGGERING ON NUMERICAL SCHEMES - Shih, Tan, Hwang'
        X = SpatialCoordinate(Z.mesh())
        (x, y, z) = X
        """ Either implement the form in the paper by Shih, Tan, Hwang and then
        use ufl to rescale, or just implement the rescaled version directly """
        # f = x**4 - 2 * x**3 + x**2
        # g = y**4 - y**2

        # from ufl.algorithms.apply_derivatives import apply_derivatives
        # df = apply_derivatives(grad(f)[0])
        # dg = apply_derivatives(grad(g)[1])
        # ddg = apply_derivatives(grad(dg)[1])
        # dddg = apply_derivatives(grad(ddg)[1])

        # F = 0.2 * x**5 - 0.5 * x**4 + (1./3.) * x**3
        # F1 = -4*x**6 + 12*x**5 - 14*x**4 + 8 * x**3 - 2*x**2
        # F2 = 0.5 * f**2
        # G1 = -24 * y**5+8*y**3-4*y
        # u = 8 * f * dg
        # v = -8 * df * g
        # p = (8./self.Re) * (F * dddg + df*dg) + 64 * F2 * (g*ddg-dg**2)
        # u = replace(u, {X: 0.5 * X})
        # v = replace(v, {X: 0.5 * X})
        # p = replace(p, {X: 0.5 * X})
        # p = p - 0.125 * assemble(p*dx)
        u = (1./4.)*(-2 + x)**2 * x**2 * y * (-2 + y**2)
        v = -(1./4.)*x*(2 - 3*x + x**2)*y**2*(-4 + y**2)
        p = -(1./128.)*(-2+x)**4*x**4*y**2*(8-2*y**2+y**4)+(x*y*(-15*x**3+3*x**4+10*x**2*y**2+20*(-2+y**2)-30*x*(-2+y**2)))/(5*self.Re)
        p = p - (-(1408./33075.) + 8./(5*self.Re))
        driver = as_vector([u, v, Constant(0)])
        return (driver, p)

    def rhs(self, Z):
        (x, y, z) = SpatialCoordinate(Z.mesh())
        (u, p) = self.actual_solution(Z)
        nu = self.char_length() * self.char_velocity() / self.Re
        f1 = -nu * div(2*sym(grad(u))) + dot(grad(u), u) + grad(p)
        f2 = -div(u)
        return f1, f2


if __name__ == "__main__":
    h = []

    import argparse
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("--nref", type=int, default=4)
    parser.add_argument("--k", type=int, default=1)
    parser.add_argument("--gamma", type=float, default=1e4)
    args, _ = parser.parse_known_args()

    base_n = 4

    # res = np.asarray([1, 10, 100, 500])
    res = [1, 10, 100, 200, 500]
    results = {}
    for re in res:
        results[re] = {}
        for s in ["velocity", "pressure"]:
            results[re][s] = []

    for nref in range(1, args.nref):
        problem = ThreeDimLidDrivenCavityMMSProblem(base_n, Re=Constant(1))
        solver = NavierStokesSolver(problem, multigrid=True, nref=nref, gamma=args.gamma)
        comm = solver.mesh.mpi_comm()

        for re in res:
            problem.Re.assign(re)

            (z, info_dict) = solver.solve(re)
            z = solver.z
            u, p = z.split()
            Z = z.function_space()

            uviz = solver.vis_velocity(u)
            (u_, p_) = problem.actual_solution(uviz.function_space())
            File("output/u-re-%i-nref-%i.pvd" % (re, nref)).write(uviz.interpolate(uviz))
            File("output/uerr-re-%i-nref-%i.pvd" % (re, nref)).write(uviz.interpolate(uviz-u_))
            (u_, p_) = problem.actual_solution(Z)
            File("output/perr-re-%i-nref-%i.pvd" % (re, nref)).write(Function(Z.sub(1)).interpolate(p-p_))
            veldiv = norm(div(u))
            pressureintegral = assemble(p_ * dx)
            uerr = errornorm(u_, u)
            perr = errornorm(p_, p)
            pinterp = p.copy(deepcopy=True).interpolate(p_)
            pinterperror = errornorm(p_, pinterp)
            pintegral = assemble(p*dx)

            results[re]["velocity"].append(uerr)
            results[re]["pressure"].append(perr)
            if comm.rank == 0:
                print("|div(u_h)| = ", veldiv)
                print("p_exact * dx = ", pressureintegral)
                print("p_approx * dx = ", pintegral)

    if comm.rank == 0:
        for re in res:
            print("Results for Re =", re)
            print("|u-u_h|", results[re]["velocity"])
            print("convergence orders:", convergence_orders(results[re]["velocity"]))
            print("|p-p_h|", results[re]["pressure"])
            print("convergence orders:", convergence_orders(results[re]["pressure"]))
        print("gamma =", args.gamma)
        print("h =", h)
