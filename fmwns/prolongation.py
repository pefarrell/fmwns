from firedrake import *
from firedrake.assemble import create_assembly_callable
from firedrake.petsc import *
import weakref
from numpy import unique
import numpy
from pyop2.datatypes import IntType
from firedrake.mg.utils import *


class CoarseCellPatches(object):
    def __call__(self, pc):
        from firedrake.mg.utils import get_level
        from firedrake.mg.impl import get_entity_renumbering

        dmf = pc.getDM()
        ctx = pc.getAttr("ctx")

        mf = ctx._x.ufl_domain()
        (mh, level) = get_level(mf)

        coarse_to_fine_cell_map = mh.coarse_to_fine_cells[level-1]
        (_, firedrake_to_plex) = get_entity_renumbering(dmf, mf._cell_numbering, "cell")

        patches = []
        for fine_firedrake in coarse_to_fine_cell_map:
            # we need to convert firedrake cell numbering to plex cell numbering
            fine_plex = [firedrake_to_plex[ff] for ff in fine_firedrake]
            entities = []
            for fp in fine_plex:
                (pts, _) = dmf.getTransitiveClosure(fp, True)
                for pt in pts:
                    value = dmf.getLabelValue("prolongation", pt)
                    if not (value > -1 and value <= level):
                        entities.append(pt)

            iset = PETSc.IS().createGeneral(unique(entities), comm=PETSc.COMM_SELF)
            patches.append(iset)

        piterset = PETSc.IS().createStride(size=len(patches), first=0, step=1, comm=PETSc.COMM_SELF)
        return (patches, piterset)


class SchoeberlProlongation(object):
    def __init__(self, Re, gamma, tdim, use_change_of_basis=False):
        self.solver = {}
        self.bcs = {}
        self.rhs = {}
        self.tensors = {}
        self.Re = Re
        self.prev_Re = {}
        self.prev_gamma = {}
        self.gamma = gamma
        self.prolong_fun = {}
        self.use_change_of_basis = use_change_of_basis

        patchparams = {"snes_type": "ksponly",
                       "ksp_type": "richardson",
                       "ksp_norm_type": "unpreconditioned",
                       "mat_type": "matfree",
                       "pc_type": "python",
                       "pc_python_type": "firedrake.PatchPC",
                       "patch_pc_patch_save_operators": "true",
                       "patch_pc_patch_partition_of_unity": False,
                       "patch_pc_patch_multiplicative": False,
                       "patch_pc_patch_sub_mat_type": "seqaij" if tdim > 2 else "seqdense",
                       "patch_pc_patch_construct_type": "python",
                       "patch_pc_patch_construct_python_type": "fmwns.prolongation.CoarseCellPatches",
                       "patch_sub_ksp_type": "preonly",
                       "patch_sub_pc_type": "lu"}
        self.patchparams = patchparams

    def break_ref_cycles(self):
        for attr in ["solver", "bcs", "rhs", "tensors", "Re", "prev_Re",
                     "prev_gamma", "gamma"]:
            if hasattr(self, attr):
                delattr(self, attr)

    @staticmethod
    def fix_coarse_boundaries(V):
        hierarchy, level = get_level(V.mesh())
        dm = V.mesh()._plex

        section = V.dm.getDefaultSection()
        indices = []
        fStart, fEnd = dm.getHeightStratum(1)
        # Spin over faces, if the face is marked with a magic label
        # value, it means it was in the coarse mesh.
        for p in range(fStart, fEnd):
            value = dm.getLabelValue("prolongation", p)
            if value > -1 and value <= level:
                # OK, so this is a coarse mesh face.
                # Grab all the points in the closure.
                closure, _ = dm.getTransitiveClosure(p)
                for c in closure:
                    # Now add all the dofs on that point to the list
                    # of boundary nodes.
                    dof = section.getDof(c)
                    off = section.getOffset(c)
                    for d in range(dof):
                        indices.append(off + d)
        nodelist = unique(indices).astype(IntType)

        class FixedDirichletBC(DirichletBC):
            def __init__(self, V, g, nodelist):
                self.nodelist = nodelist
                DirichletBC.__init__(self, V, g, "on_boundary")

            @utils.cached_property
            def nodes(self):
                return self.nodelist

        dim = V.mesh().topological_dimension()
        bc = FixedDirichletBC(V,  dim * (0, ), nodelist)

        return bc

    def prolong(self, coarse, fine):
        # Rebuild without any indices
        V = FunctionSpace(fine.ufl_domain(), fine.function_space().ufl_element())
        key = V.dim()

        firsttime = self.bcs.get(key, None) is None

        prev_Re = self.prev_Re.get(key, None)
        prev_gamma = self.prev_gamma.get(key, None)

        Re = self.Re
        gamma = self.gamma

        if firsttime:
            bcs = self.fix_coarse_boundaries(V)
            u = TrialFunction(V)
            v = TestFunction(V)
            A = assemble(1.0/Re * inner(2*sym(grad(u)), grad(v))*dx + gamma*inner(cell_avg(div(u)), div(v))*dx, bcs=bcs, mat_type=self.patchparams["mat_type"])

            tildeu, rhs = Function(V), Function(V)

            bform = 1.0/Re * inner(2*sym(grad(rhs)), grad(v))*dx + gamma*inner(cell_avg(div(rhs)), div(v))*dx
            b = Function(V)

            solver = LinearSolver(A, solver_parameters=self.patchparams,
                                  options_prefix="prolongation")
            self.bcs[key] = bcs
            self.solver[key] = solver
            self.rhs[key] = tildeu, rhs
            self.tensors[key] = A, b, bform
            if V.mesh().topological_dimension() == 3 and "CG1" in V.ufl_element().shortstr():
                prolongator = MagicProlongator(coarse.function_space(), fine.function_space(),
                                               use_change_of_basis=self.use_change_of_basis)
                self.prolong_fun[key] = prolongator.prolong
            else:
                self.prolong_fun[key] = prolong
            prolong_fun = self.prolong_fun[key]
        else:
            bcs = self.bcs[key]
            solver = self.solver[key]
            A, b, bform = self.tensors[key]
            tildeu, rhs = self.rhs[key]
            prolong_fun = self.prolong_fun[key]

        # Update operator if parameters have changed.
        if float(self.Re) != prev_Re or float(self.gamma) != prev_gamma:
            A = solver.A
            A = assemble(A.a, bcs=bcs, mat_type=self.patchparams["mat_type"], tensor=A)
            A.force_evaluation()
            self.prev_Re[key] = float(self.Re)
            self.prev_gamma[key] = float(self.gamma)

        prolong_fun(coarse, rhs)

        b = assemble(bform, bcs=bcs, tensor=b)
        # # Could do
        # #solver.solve(tildeu, b)
        # # but that calls a lot of SNES and KSP overhead.
        # # We know we just want to apply the PC:
        with solver.inserted_options():
            with b.dat.vec_ro as rhsv:
                with tildeu.dat.vec_wo as x:
                    solver.ksp.pc.apply(rhsv, x)
        fine.assign(rhs - tildeu)

        # def energy_norm(u):
        #     return assemble(1.0/Re * inner(grad(u), grad(u)) * dx + gamma * inner(cell_avg(div(u)), div(u)) * dx)
        # def H1_norm(u):
        #     return assemble(1.0/Re * inner(grad(u), grad(u)) * dx)

        # warning("\|coarse\| %f " % energy_norm(coarse))
        # warning("\|coarse\|_1 %f " % H1_norm(coarse))
        # warning("\|fine\| %f" % energy_norm(fine))
        # warning("\|fine\|_1 %f" % H1_norm(fine))
        # warning("\|tildeu\| %f" % energy_norm(tildeu))
        # import sys; sys.exit(1)


class MagicProlongator(object):
    def __init__(self, Vc, Vf, use_change_of_basis=False):
        meshc = Vc.mesh()
        meshf = Vf.mesh()
        P1c = VectorFunctionSpace(meshc, "CG", 1)
        FBc = VectorFunctionSpace(meshc, "FacetBubble", 3)
        P1f = VectorFunctionSpace(meshf, "CG", 1)
        FBf = VectorFunctionSpace(meshf, "FacetBubble", 3)

        self.p1c = Function(P1c)
        self.fbc = Function(FBc)
        self.p1f = Function(P1f)
        self.fbf = Function(FBf)
        self.rhs = Function(FBc)

        trial = TrialFunction(FBc)
        test = TestFunction(FBc)
        n = FacetNormal(meshc)
        a = inner(inner(trial, n), inner(test, n)) + inner(trial - inner(trial, n)*n, test-inner(test, n)*n)
        a = a*ds + avg(a)*dS
        A = assemble(a).M.handle.getDiagonal()
        # A is diagonal, so "solve" by dividing by the diagonal.
        ainv = A.copy()
        ainv.reciprocal()
        self.ainv = ainv
        L = inner(inner(self.fbc, n)/0.625, inner(test, n)) + inner(self.fbc - inner(self.fbc, n)*n, test-inner(test, n)*n)
        L = L*ds + avg(L) * dS
        # This is the guts of assemble(L, tensor=self.rhs), but saves a little time
        self.assemble_rhs = create_assembly_callable(L, tensor=self.rhs)
        self.use_change_of_basis = use_change_of_basis

        # TODO: Parameterise these by the element definition.
        # These are correct for P1 + FB in 3D.
        # Dof layout for the combined element on the reference cell is:
        # 4 vertex dofs, then 4 face dofs.
        if use_change_of_basis:
            """
            These two kernels perform a change of basis from a nodal to a
            hierachichal basis ("split") and then back from a hierachichal
            basis to a nodal basis ("combine").  The nodal functionspace is
            given by P1FB, the hierachichal one is given by two spaces: P1 and
            FB. The splitting kernel is defined so that
                [a b] * [p1fb_1; ...;p1fb_8] = [p1_1;...;p1_4;fb_1;...;fb_4]
            and the combine kernel is defined so that
                [a;b] * [p1_1;...;p1_4;fb_1;...;fb_4] = [p1fb_1;...;p1fb_8]

            Notation: [x;y]: vertically stack x and y, [x y]: horizontally stacked
            """
            self.split_kernel = op2.Kernel("""
void split(double p1[12], double fb[12], const double both[24]) {
  for (int i = 0; i < 12; i++) {
    p1[i] = 0;
    fb[i] = 0;
  }

  double a[8][4] = {{1., 0., 0., 0.},
                    {0., 1., 0., 0.},
                    {0., 0., 1., 0.},
                    {0., 0., 0., 1.},
                    {0., 0., 0., 0.},
                    {0., 0., 0., 0.},
                    {0., 0., 0., 0.},
                    {0., 0., 0., 0.}};

  double b[8][4] = {{0., -1./3, -1./3, -1./3},
                    {-1./3, 0., -1./3, -1./3},
                    {-1./3, -1./3, 0., -1./3},
                    {-1./3, -1./3, -1./3, 0.},
                    {1., 0., 0., 0.},
                    {0., 1., 0., 0.},
                    {0., 0., 1., 0.},
                    {0., 0., 0., 1.}};

  for (int k = 0; k < 8; k++) {
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 3; j++) {
        p1[3 * i + j] += a[k][i] * both[3 * k + j];
        fb[3 * i + j] += b[k][i] * both[3 * k + j];
      }
    }
  }
}""", "split")
            self.combine_kernel = op2.Kernel("""
void combine(const double p1[12], const double fb[12], double both[24]) {
  for (int i = 0; i < 24; i++) both[i] = 0;

  double a[4][8] = {{1., 0., 0., 0., 0., 1./3, 1./3, 1./3},
                    {0., 1., 0., 0., 1./3, 0., 1./3, 1./3},
                    {0., 0., 1., 0., 1./3, 1./3, 0., 1./3},
                    {0., 0., 0., 1., 1./3, 1./3, 1./3, 0.}};

  double b[4][8] = {{0., 0., 0., 0., 1., 0., 0., 0.},
                    {0., 0., 0., 0., -0., 1., 0., 0.},
                    {0., 0., 0., 0., -0., -0., 1., 0.},
                    {0., 0., 0., 0., -0., -0., -0., 1.}};

  for (int k = 0; k < 4; k++) {
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 3; j++) {
        both[3 * i + j] += a[k][i] * p1[3 * k + j];
        both[3 * i + j] += b[k][i] * fb[3 * k + j];
      }
    }
  }
}
""", "combine")
        else:
            self.split_kernel = op2.Kernel("""
void split(double p1[12], double fb[12], const double both[24]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 3; j++) {
            p1[3*i + j] = both[3*i + j];
            fb[3*i + j] = both[3*(i+4) + j];
        }
    }
}""", "split")
            self.combine_kernel = op2.Kernel("""
void combine(const double p1[12], const double fb[12], double both[24]) {
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 3; j++) {
      both[3*i + j] = p1[3*i + j];
      both[3*(i+4) + j] = fb[3*i + j];
    }
  }
}""", "combine")


    def prolong(self, coarse, fine):
        # Step 1: perform a change of basis into a piecewise linear and into a bubble function
        op2.par_loop(self.split_kernel, coarse.ufl_domain().cell_set,
                     self.p1c.dat(op2.WRITE, self.p1c.cell_node_map()),
                     self.fbc.dat(op2.WRITE, self.fbc.cell_node_map()),
                     coarse.dat(op2.READ, coarse.cell_node_map()))

        # Step 2: Piecewise linear functions are prolonged correctly, but for bubble functions
        # we make a mistake. This is because the bubble function can't be represented perfectly.
        # It turns out the mistake is exactly that we underestimate the flux across the coarse facets
        # by a factor of 0.625. To fix this, we scale up the normal components of the bubble functions,
        # while keeping the tangential components unchanged.
        self.assemble_rhs()
        with self.fbc.dat.vec_wo as fbc, self.rhs.dat.vec_ro as b:
            fbc.pointwiseMult(b, self.ainv)

        # Step 3: Now prolong the two functions and then combine back together again.
        prolong(self.p1c, self.p1f)
        prolong(self.fbc, self.fbf)
        op2.par_loop(self.combine_kernel, fine.ufl_domain().cell_set,
                     self.p1f.dat(op2.READ, self.p1f.cell_node_map()),
                     self.fbf.dat(op2.READ, self.fbf.cell_node_map()),
                     fine.dat(op2.WRITE, fine.cell_node_map()))
