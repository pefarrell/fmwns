from firedrake import *
from fmwns import *

import os
import shutil


class TwoDimBackwardsFacingStepProblem(NavierStokesProblem):
    def __init__(self, msh):
        self.msh = msh
        super().__init__()

    def mesh(self, distribution_parameters):
        return Mesh(self.msh, distribution_parameters=distribution_parameters)

    @staticmethod
    def poiseuille_flow(domain):
        (x, y) = SpatialCoordinate(domain)
        return as_vector([4 * (2-y)*(y-1)*(y>1), 0])

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), self.poiseuille_flow(Z.mesh()), 1),
               DirichletBC(Z.sub(0), Constant((0., 0.)), 2)]
        return bcs

    def has_nullspace(self):
        return False


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("--nref", type=int, default=1)
    parser.add_argument("--clear", dest="clear", default=False, action="store_true")
    parser.add_argument("--mesh", type=str, default="coarseA.msh")
    parser.add_argument("--supg", type=float, default=None)
    args, _ = parser.parse_known_args()

    if args.clear:
        shutil.rmtree("checkpoint", ignore_errors=True)

    # user has to create mesh first by running make first
    problem = TwoDimBackwardsFacingStepProblem(args.mesh)
    solver = NavierStokesSolver(problem, multigrid=True, nref=args.nref, gamma=10000, nref_viz=1, supg_weight=Constant(args.supg))
    comm = solver.mesh.mpi_comm()

    upvdf = File("output/velocity-%i.pvd" % solver.Z.dim())
    ppvdf = File("output/pressure-%i.pvd" % solver.Z.dim())
    os.makedirs("checkpoint", exist_ok=True)
    start = 200
    end = 10000
    step = 100
    res = [0, 10, 100] + list(range(start, end+step, step))
    results = []
    for re in res:
        try:
            with DumbCheckpoint("checkpoint/nssolution-dofs-%s-Re-%s" % (solver.Z.dim(), re), mode=FILE_READ) as checkpoint:
                checkpoint.load(solver.z, name="up_%i" % re)
            upvdf.write(solver.vis_velocity(solver.z.split()[0]), time=re)
            ppvdf.write(solver.z.split()[1], time=re)
            continue
        except:
            pass
        (z, info_dict) = solver.solve(re)
        results.append(info_dict)
        with DumbCheckpoint("checkpoint/nssolution-dofs-%s-Re-%s" % (solver.Z.dim(), re), mode=FILE_UPDATE) as checkpoint:
            checkpoint.store(solver.z, name="up_%i" % re)
        upvdf.write(solver.vis_velocity(solver.z.split()[0]), time=re)
        ppvdf.write(solver.z.split()[1], time=re)

    # Otherwise, some things are not collected (This lives inside a
    # DM, but holds a reference to the DM itself)
    solver.prolongation.break_ref_cycles()
    if comm.rank == 0:
        for info_dict in results:
            print(info_dict)
