from setuptools import setup

setup(name="fmwns",
      version="0.0.2",
      description="Solvers for the stationary incompressible Navier-Stokes equations",
      packages = ["fmwns"])
