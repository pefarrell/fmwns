from firedrake import *

def velocity_element(mesh, vel_degree_wo_bubble=1):
    tdim = mesh.topological_dimension()
    if vel_degree_wo_bubble < tdim:
        Pk = FiniteElement("Lagrange", mesh.ufl_cell(), vel_degree_wo_bubble)
        FB = FiniteElement("FacetBubble", mesh.ufl_cell(), tdim)
        ele = VectorElement(NodalEnrichedElement(Pk, FB))
    else:
        Pk = FiniteElement("Lagrange", mesh.ufl_cell(), vel_degree_wo_bubble)
        ele = VectorElement(Pk)
    return ele

def pressure_element(mesh):
    return FiniteElement("Discontinuous Lagrange", mesh.ufl_cell(), 0)
