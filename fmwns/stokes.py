from firedrake import *
from firedrake.petsc import *

from datetime import datetime

def solve_stokes(mesh, problem, Z):
    V = Z.sub(0)
    Q = Z.sub(1)

    z = Function(Z)
    (u, p) = split(z)
    (v, q) = TestFunctions(Z)

    bcs = problem.bcs(Z)

    F = inner(grad(u), grad(v)) * dx - p * div(v) * dx - div(u) * q * dx
    nsp = problem.nullspace(Z)
    # Parameters that use a bit less memory
    fieldsplit_0_params_hypre_lomem = {
        "ksp_type": "cg",
        "ksp_max_it": 5,
        "pc_type": "hypre",
        "pc_hypre_boomeramg_P_max": 4,
        "pc_hypre_boomeramg_no_CF": None,
        "pc_hypre_boomeramg_agg_nl": 1,
        "pc_hypre_boomeramg_agg_num_paths": 2,
        "pc_hypre_boomeramg_coarsen_type": "HMIS",
        "pc_hypre_boomeramg_interp_type": "ext+i",
    }
    # stronger but needs more memory
    fieldsplit_0_params_hypre_himem = {
        "ksp_type": "cg",
        "ksp_max_it": 5,
        "pc_type": "hypre",
    }

    size = mesh.mpi_comm().size
    if size > 24:
        telescope_factor = round(size/24.0)
    else:
        telescope_factor = 1

    fieldsplit_0_params_gmg_matfree = {
        "ksp_type": "cg",
        "ksp_max_it": 5,
        "pc_type": "mg",
        "ksp_monitor": None,
        "mg_levels": {
            "ksp_type": "richardson",
            "ksp_richardson_self_scale": True,
            "pc_type": "python",
            "pc_python_type": "firedrake.AssembledPC",
            "assembled_mat_type": "baij",
            "assembled_pc_type": "sor",
        },
        "mg_coarse": {
            "ksp_type": "richardson",
            "pc_type": "python",
            "pc_python_type": "firedrake.AssembledPC",
            "assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_reduction_factor": telescope_factor,
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "mumps",
            }
        },
    }

    fieldsplit_0_params_gmg_matnest = {
        "ksp_type": "cg",
        "ksp_max_it": 5,
        "pc_type": "mg",
        "mg_coarse": {
            "pc_type": "lu",
            "pc_factor_mat_solver_package": "mumps"
        },
    }

    params = {
        "mat_type": "matfree",
        #"mat_type": "nest",
        "ksp_monitor": None,
        "ksp_type": "fgmres",
        "ksp_max_it": 100,
        # "ksp_monitor": None,
        "snes_type": "ksponly",
        "pc_type": "fieldsplit",  # block factorisation
        "pc_fieldsplit_type": "schur",
        "pc_fieldsplit_schur_factorization_type": "full",
        "pc_fieldsplit_schur_precondition": "user",
        #"fieldsplit_0": fieldsplit_0_params_gmg_matnest,
        "fieldsplit_0": fieldsplit_0_params_gmg_matfree,
        #"fieldsplit_0": fieldsplit_0_params_hypre_himem,
        #"fieldsplit_0": fieldsplit_0_params_hypre_lomem,
        "fieldsplit_1": {
            "ksp_type": "cg",
            "ksp_max_it": 5,
            "pc_type": "mat",
        }
    }
    nlproblem = NonlinearVariationalProblem(F, z, bcs)
    nlsolver = NonlinearVariationalSolver(nlproblem, nullspace=nsp,
                                          solver_parameters=params)
    snes = nlsolver.snes
    mass_test = TestFunction(Q)

    inv_mass = assemble((mass_test/(CellVolume(Q.mesh())**2))*dx)

    class SchurInvApprox(object):
        def mult(self, mat, x, y):
            with inv_mass.dat.vec_ro as w:
                y.pointwiseMult(x, w)
            y.scale(-1)

    schur = PETSc.Mat()
    lSize = inv_mass.vector().local_size()
    gSize = inv_mass.vector().size()
    schur.createPython(((lSize, gSize), (lSize, gSize)), SchurInvApprox())
    schur.setUp()
    snes.ksp.pc.setFieldSplitSchurPreType(PETSc.PC.SchurPreType.USER, schur)

    start = datetime.now()
    nlsolver.solve()
    end = datetime.now()
    Re_linear_its = snes.getLinearSolveIterations()
    Re_time = (end-start).total_seconds() / 60
    info_dict = {
        "Re": 0,
        "nu": float("inf"),
        "linear_iter": Re_linear_its,
        "nonlinear_iter": 1,
        "time": Re_time,
    }
    return z, info_dict
